package id.co.altama.cgen.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "oauth_approvals")
@EqualsAndHashCode
@ToString
@Data
public class OauthApprovals implements Serializable {

	private static final long serialVersionUID = 1L;

	// token_id, token, authentication_id, user_name, client_id, authentication,
	// refresh_token)
	@Id	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Setter
	@Getter
	@Column(name = "id", nullable = false)
	private Integer id;
	
	@Setter
	@Getter
	@Column(name = "\"userId\"", length = 256)
	private String userId;

	@Setter
	@Getter
	@Column(name = "\"clientId\"", length = 256)
	private String clientId;

	@Setter
	@Getter
	@Column(name = "scope", length = 256)
	private String scope;

	@Setter
	@Getter
	@Column(name = "status", length = 10)
	private String status;

	@Setter
	@Getter
	@Column(name = "\"expiresAt\"")
	@Temporal(TemporalType.TIMESTAMP)
	private Date expiresAt;
	
	@Setter
	@Getter
	@Column(name = "\"lastModifiedAt\"")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastModifiedAt;

	
}
