package id.co.altama.cgen.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "oauth_access_token")
@EqualsAndHashCode
@ToString
@Data
public class OauthAccessToken implements Serializable {

	private static final long serialVersionUID = 1L;

	// token_id, token, authentication_id, user_name, client_id, authentication,
	// refresh_token)
	@Id
	@Setter
	@Getter
	@Column(name = "authentication_id", length = 256)
	private String authenticationId;
	
	
	@Setter
	@Getter
	@Column(name = "token_id", length = 256)
	private String tokenId;

	@Setter
	@Getter
	@Column(name = "token")
	private byte[] token;

	

	@Setter
	@Getter
	@Column(name = "user_name", length = 256)
	private String userName;

	@Setter
	@Getter
	@Column(name = "client_id", length = 256)
	private String clientId;

	@Setter
	@Getter
	@Column(name = "authentication")
	private byte[]  authentication;

	@Setter
	@Getter
	@Column(name = "refresh_token", length = 256)
	private String refreshToken;
}
