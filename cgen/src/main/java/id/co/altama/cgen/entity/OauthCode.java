package id.co.altama.cgen.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "oauth_code")
@EqualsAndHashCode
@ToString
@Data
public class OauthCode implements Serializable {

	private static final long serialVersionUID = 1L;

	// token_id, token, authentication_id, user_name, client_id, authentication,
	// refresh_token)
	@Id	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Setter
	@Getter
	@Column(name = "id", nullable = false)
	private Integer id;
	
	
	@Setter
	@Getter
	@Column(name = "authentication")
	private byte[]  authentication;
	
	@Setter
	@Getter
	@Column(name = "code", length = 256)
	private String code;

	
}
