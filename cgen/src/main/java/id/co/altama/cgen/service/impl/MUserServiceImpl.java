package id.co.altama.cgen.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.altama.cgen.dao.MUserDao;
import id.co.altama.cgen.service.MUserService;

@Service("mUserService")
public class MUserServiceImpl implements MUserService {

	private static Logger logger = LogManager.getLogger(MUserServiceImpl.class);

	@Autowired
	private MUserDao mUserDao;

	@Override
	public boolean setDefaultUser() {
		logger.info("Try set default User");
		long l = mUserDao.count();
		logger.info("User available={}", l);
		if (l != 0) {
			logger.info("User is not empty");
			return true;
		}
		logger.info("Try insert user!");
		// ditambahkan!

		return true;
	}

}
