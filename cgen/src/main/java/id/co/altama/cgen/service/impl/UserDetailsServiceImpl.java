package id.co.altama.cgen.service.impl;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import id.co.altama.cgen.dao.MUserDao;
import id.co.altama.cgen.entity.MUser;
import id.co.altama.cgen.service.MUserService;

@Service(value = "userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	MUserDao mUserDao;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		MUser mUser = mUserDao.findByUsername(username);

		if (mUser != null) {
			User user = new User(mUser.getUserName(), mUser.getPassword(), mUser.getEnabled(),
					!mUser.getAccount_expired(), !mUser.getCredentialsExpired(), !mUser.getAccount_locked(),
					getAuthority());
			return user;

		}

		throw new UsernameNotFoundException(username);
	}

	private List getAuthority() {
		return Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
	}

}
