package id.co.altama.cgen.service;

import id.co.altama.cgen.model.RestResponse;

public interface MTableService {

	public RestResponse getCurrentTimestampDatabase();

}
