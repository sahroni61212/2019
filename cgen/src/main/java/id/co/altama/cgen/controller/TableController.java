package id.co.altama.cgen.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import id.co.altama.cgen.model.RestResponse;

@RestController
@RequestMapping("/table/")
public class TableController {
	
	private static Logger logger = LogManager.getLogger(TableController.class);

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/test", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity test() {
		logger.info("::::::TEST:::::");
		return new ResponseEntity<>(new RestResponse(RestResponse.OK_REST_STATUS, "TEST"), HttpStatus.OK);
	}
}
