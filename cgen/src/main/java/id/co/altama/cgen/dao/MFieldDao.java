package id.co.altama.cgen.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.altama.cgen.entity.MField;
import id.co.altama.cgen.entity.MFieldPK;

public interface MFieldDao extends JpaRepository<MField, MFieldPK> {

	@Query("SELECT a FROM MField a WHERE a.tableName = :tableName")
	public List<MField> findByTable(@Param("tableName") String tableName);

}
