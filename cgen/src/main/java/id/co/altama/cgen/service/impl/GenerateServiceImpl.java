package id.co.altama.cgen.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.altama.cgen.dao.MFieldDao;
import id.co.altama.cgen.dao.MTableDao;
import id.co.altama.cgen.dto.MFieldDto;
import id.co.altama.cgen.dto.MTableDto;
import id.co.altama.cgen.entity.MField;
import id.co.altama.cgen.entity.MTable;
import id.co.altama.cgen.ftl.ConfigTemplate;
import id.co.altama.cgen.service.GenerateService;
import id.co.altama.cgen.util.ConstantUtil;
import id.co.altama.cgen.util.JsonUtil;

@Service("generateService")
@Transactional(readOnly = true)
public class GenerateServiceImpl extends ConfigTemplate implements GenerateService {

	private static Logger logger = LogManager.getLogger(GenerateServiceImpl.class);

	@Autowired
	private MFieldDao mFieldDao;

	@Autowired
	private MTableDao mTableDao;

	@Value("${package.entity}")
	private String packageEntity;

	@Value("${package.dto}")
	private String packageDto;

	@Value("${path.generate.default}")
	private String pathGenerateDefault;


	@Override
	public String generateDto(String... tableNames) throws Exception {
		logger.info(":::Start Service Generate DTO:::");
		logger.info(":::init={}:::", JsonUtil.getJson(tableNames));
		return generateDefaultDto("dto-template.ftl", tableNames);
	}

	private String generateDefaultDto(String templateName, String... tableNames) throws Exception {
		logger.info(":::Do Generate DTO:::");
		logger.info(":::init=> tableNames={}, templateName={}:::", JsonUtil.getJson(tableNames), templateName);
		List<MTableDto> l = getMtableDtos(tableNames);
		if (l != null) {
			for (MTableDto d : l) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("package", packageDto);
				map.put("mTableDto", d);
				map.put("mFieldDtos", d.getMFieldDtos());
				doGenerate(map, (pathGenerateDefault + d.getTableNameJava() + "Dto.java"), templateName);
			}
		}

		return "Done";
	}

	@Override
	public String generateDtoLombok(String... tableNames) throws Exception {
		logger.info(":::Start Service Generate DTO LOMBOK:::");
		logger.info(":::init={}:::", JsonUtil.getJson(tableNames));
		return generateDefaultDto("dto-lombok-template.ftl", tableNames);
	}
	

	@Override
	public String generateEntityHibernate(String... tableNames) throws Exception {
		logger.info(":::Start Service Generate Entity Hibernate:::");
		logger.info(":::init={}:::", JsonUtil.getJson(tableNames));		
		return generateDefaultEntity("entity-hibernate-template.ftl", tableNames);
	}
	
	private String generateDefaultEntity(String templateName, String... tableNames) throws Exception {
		logger.info(":::Start Service Generate Entity:::");
		logger.info(":::init=> tableNames={}, templateName={}:::", JsonUtil.getJson(tableNames), templateName);
		List<MTableDto> l = getMtableDtos(tableNames);
		if (l != null) {
			for (MTableDto d : l) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("package", packageEntity);
				map.put("mTableDto", d);
				map.put("mFieldDtos", d.getMFieldDtos());
				doGenerate(map, (pathGenerateDefault + d.getTableNameJava() + ".java"), templateName);
			}
		}
		return "Done";
	}
	
	@Override
	public String generateEntityHibernateLombok(String... tableNames) throws Exception {
		logger.info(":::Start Service Generate Entity Hibernate Lombok:::");
		logger.info(":::init={}:::", JsonUtil.getJson(tableNames));		
		return generateDefaultEntity("entity-hibernate-lombok-template.ftl", tableNames);
	}

	private List<MTableDto> getMtableDtos(String... tableNames) throws Exception {
		logger.info(":::init={}:::", JsonUtil.getJson(tableNames));
		List<MTableDto> mTableDtos = new ArrayList<MTableDto>();
		List<MTable> l = null;
		if (tableNames == null) {
			logger.info(":::Get ALL Table:::");
			l = mTableDao.findAll();
		} else {
			List<String> s = Arrays.asList(tableNames);
			logger.info(":::Get Table By IDs={}:::", JsonUtil.getJson(tableNames));
			l = mTableDao.findAllById(s);
		}
		if (l != null) {
			mTableDtos = JsonUtil.mapJsonToListObject(l, MTableDto.class);
			for (MTableDto dto : mTableDtos) {
				logger.info(":::Get Field By Table={}:::", dto.getTableName());
				List<MField> fs = mFieldDao.findByTable(dto.getTableName());
				List<MFieldDto> mFieldDtos = JsonUtil.mapJsonToListObject(fs, MFieldDto.class);
				int countPk = 0;
				for (MFieldDto f : mFieldDtos) {
					if(ConstantUtil.Y.equals(f.getPrimaryKey())) {
						countPk = countPk + 1;
					}
				}
				dto.setCountPk(countPk);
				dto.setMFieldDtos(mFieldDtos);
			}

		}
		logger.info(":::Final data table={}:::", JsonUtil.getJson(mTableDtos));
		return mTableDtos;
	}

	

}
