package id.co.altama.cgen.ftl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

import freemarker.core.ParseException;
import freemarker.template.Configuration;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.TemplateNotFoundException;

public abstract class ConfigTemplate {

	private static Logger logger = LogManager.getLogger(ConfigTemplate.class);

	@Value("${template.files}")
	private String templateFiles;

	protected Configuration getConfig() throws IOException {
		Configuration cfg = new Configuration(Configuration.VERSION_2_3_21);
		cfg.setDirectoryForTemplateLoading(new File(templateFiles));
		cfg.setDefaultEncoding("UTF-8");
		cfg.setTemplateExceptionHandler(TemplateExceptionHandler.HTML_DEBUG_HANDLER);
		return cfg;
	}

	public String doGenerate(Object data, String fileName, String templateName) {
		Writer out = null;
		try {
			File file = new File(fileName);
			if (!file.exists()) {
				file.getParentFile().mkdirs();
				file.createNewFile();
			}
			out = new FileWriter(file.getAbsoluteFile());
			getTemplate(templateName).process(data, out);
			out.flush();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} finally {
			try {
				out.close();
				out = null;
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
			}
		}

		return "Done";
	}

	protected Template getTemplate(String templateName)
			throws TemplateNotFoundException, MalformedTemplateNameException, ParseException, IOException, Exception {
		return getConfig().getTemplate(templateName);
	}

//	protected String getTemplateName() throws Exception {
//		throw new Exception("TemplateName must be implement.");
//	}
//
//	protected String getPath() throws Exception {
//		throw new Exception("path must be implement.");
//	}

}
