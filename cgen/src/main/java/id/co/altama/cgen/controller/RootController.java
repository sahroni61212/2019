package id.co.altama.cgen.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import id.co.altama.cgen.model.RestResponse;
import id.co.altama.cgen.service.MTableService;

@RestController
@RequestMapping("/")
public class RootController {

	private static Logger logger = LogManager.getLogger(RootController.class);
	
	@Autowired private MTableService mTableService;

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/welcome", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity welcome() {
		logger.info("::::::WELCOME:::::");
		return new ResponseEntity<>(new RestResponse(RestResponse.OK_REST_STATUS, "Welcome"), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/check_connection", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public RestResponse check_connection() {
		logger.info("::::::check_connection:::::");
		return mTableService.getCurrentTimestampDatabase();
	}
}
