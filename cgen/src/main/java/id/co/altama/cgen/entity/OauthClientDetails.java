package id.co.altama.cgen.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "oauth_client_details")
@EqualsAndHashCode
@ToString
@Data
public class OauthClientDetails implements Serializable {

	private static final long serialVersionUID = 1L;

	// token_id, token, authentication_id, user_name, client_id, authentication,
	// refresh_token)

	@Id
	@Setter
	@Getter
	@Column(name = "client_id", length = 256)
	private String clientId;

	@Setter
	@Getter
	@Column(name = "resource_ids",  length = 256)
	private String resourceIds;

	@Setter
	@Getter
	@Column(name = "client_secret",  length = 256)
	private String clientSecret;

	@Setter
	@Getter
	@Column(name = "scope", length = 256)
	private String scope;

	@Setter
	@Getter
	@Column(name = "authorized_grant_types", length = 256)
	private String authorizedGrantTypes;

	@Setter
	@Getter
	@Column(name = "web_server_redirect_uri", length = 256)
	private String webServerRedirectUri;

	@Setter
	@Getter
	@Column(name = "authorities", length = 256)
	private String authorities;
	
	
	@Setter
	@Getter
	@Column(name = "access_token_validity")
	private Integer accessTokenValidity;

	@Setter
	@Getter
	@Column(name = "refresh_token_validity")
	private Integer refreshTokenValidity;
	
	
	@Setter
	@Getter
	@Column(name = "additional_information", length = 4096)
	private String additionalInformation;

	@Setter
	@Getter
	@Column(name = "autoapprove", length = 256)
	private String autoapprove;
	
	@Setter
	@Getter
	@Column(name = "api_key",  length = 256)
	private String apiKey;
	
	@Setter
	@Getter
	@Column(name = "api_secret",  length = 256)
	private String apiSecret;
	
	@Setter
	@Getter
	@Column(name = "user_name",  length = 256)
	private String userName;
}
