package ${package};


import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 *
 *@author Sahroni
 *Menggunakan Generate
 *Pada ${.now?string["dd MMM yyyy HH:mm:ss"]}
 */

public class ${mTableDto.tableNameJava?cap_first} implements Serializable {

  private static final long serialVersionUID = 1L;
	
  <#list mFieldDtos as mFieldDto>
  private <#if mFieldDto.dataType == 'Datetime'>Date<#elseif mFieldDto.dataType == 'Number'>BigDecimal<#else>${mFieldDto.dataType }</#if> ${mFieldDto.fieldNameJava?uncap_first};
	
  </#list>
	
		
	<#list mFieldDtos as mFieldDto>
  public <#if mFieldDto.dataType == 'Datetime'>Date<#elseif mFieldDto.dataType == 'Number'>BigDecimal<#else>${mFieldDto.dataType }</#if> get${mFieldDto.fieldNameJava?cap_first}() { 
    return this.${mFieldDto.fieldNameJava?uncap_first}; 
  }	
	
  public void set${mFieldDto.fieldNameJava?cap_first}(<#if mFieldDto.dataType == 'Datetime'>Date<#elseif mFieldDto.dataType == 'Number'>BigDecimal<#else>${mFieldDto.dataType }</#if> ${mFieldDto.fieldNameJava?uncap_first}) {
    this.${mFieldDto.fieldNameJava?uncap_first} = ${mFieldDto.fieldNameJava?uncap_first};
  }
	
	</#list>	

}