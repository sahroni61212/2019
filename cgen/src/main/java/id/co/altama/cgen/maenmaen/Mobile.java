package id.co.altama.cgen.maenmaen;

public class Mobile {

	String warna;
	
	int cc;
	
	String transmisi;
	
	public Mobile() {
		
	}

	public Mobile(String warna,  String transmisi) {
		super();
		this.warna = warna;
		this.transmisi = transmisi;
	}
	
	public Mobile(String warna, int cc, String transmisi) {
		super();
		this.warna = warna;
		this.cc = cc;
		this.transmisi = transmisi;
	}
	
	
}
