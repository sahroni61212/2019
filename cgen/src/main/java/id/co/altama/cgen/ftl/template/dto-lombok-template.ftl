package ${package};


import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 *
 *@author Sahroni
 *Menggunakan Generate
 *Pada ${.now?string["dd MMM yyyy HH:mm:ss"]}
 */

@EqualsAndHashCode
@ToString
public class ${mTableDto.tableNameJava?cap_first} implements Serializable {

  private static final long serialVersionUID = 1L;
	
  <#list mFieldDtos as mFieldDto>
  @Setter
  @Getter
  private <#if mFieldDto.dataType == 'Datetime'>Date<#elseif mFieldDto.dataType == 'Number'>BigDecimal<#else>${mFieldDto.dataType }</#if> ${mFieldDto.fieldNameJava?uncap_first};
	
  </#list>
	
}