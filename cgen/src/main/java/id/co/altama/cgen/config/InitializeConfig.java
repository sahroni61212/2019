package id.co.altama.cgen.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import id.co.altama.cgen.service.GenerateService;

@Component
public class InitializeConfig implements InitializingBean {

	private static Logger logger = LogManager.getLogger(InitializeConfig.class);

	@Autowired
	private GenerateService generateService;

	@Value("${template.files}")
	private String templateFiles;

	@Override
	public void afterPropertiesSet() throws Exception {
		logger.info(":::InitializeConfig:::");
//		GenerateTest.get().buildData().writeFile();
		System.out.println("templateFiles=" + templateFiles);
//		generateService.generateDto("Test");
		generateService.generateEntityHibernate("Test");
	}

}
