package id.co.altama.cgen.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "M_Table")
@EqualsAndHashCode
@ToString
@Data
public class MTable implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Setter
	@Getter
	@Column(name = "table_name", nullable = false, length = 50, unique = true)
	private String tableName;

	@Setter
	@Getter
	@Column(name = "table_name_java", nullable = false, length = 50)
	private String tableNameJava;

	@Setter
	@Getter
	@Column(name = "created_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate = new Date();
	
	@Setter
	@Getter
	@Column(name = "created_by", length = 50)
	private String created_by;
	
	@Setter
	@Getter
	@Column(name = "updated_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedDate;

	@Setter
	@Getter
	@Column(name = "updated_by", length = 50)
	private String updatedBy;

}
