package ${package};


import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 *
 *@author Sahroni
 *Menggunakan Generate
 *Pada ${.now?string["dd MMM yyyy HH:mm:ss"]}
 */

@Entity
@Table(name = "${mTableDto.tableName}")
<#if mTableDto.countPk gt 1>@IdClass(value=${mTableDto.tableNameJava?cap_first}PK.class)</#if>
public class ${mTableDto.tableNameJava?cap_first} implements Serializable {

  private static final long serialVersionUID = 1L;
	
  <#list mFieldDtos as mFieldDto>  
  <#if mFieldDto.primaryKey == 'Y'>@Id</#if>
  <#if mFieldDto.dataType == 'Datetime'>@Temporal(TemporalType.TIMESTAMP)<#elseif mFieldDto.dataType == 'Date'>@Temporal(TemporalType.DATE)</#if>
  @Column(name = "${mFieldDto.fieldName}", nullable = <#if mFieldDto.nullable == 'Y'>true<#else>false</#if> <#if mFieldDto.dataType == 'String'>, length = ${mFieldDto.length}</#if>)
  private <#if mFieldDto.dataType == 'Datetime'>Date<#elseif mFieldDto.dataType == 'Number'>BigDecimal<#else>${mFieldDto.dataType }</#if> ${mFieldDto.fieldNameJava?uncap_first};
  ${'\n'}
  </#list>
		
	<#list mFieldDtos as mFieldDto>
	
  public <#if mFieldDto.dataType == 'Datetime'>Date<#elseif mFieldDto.dataType == 'Number'>BigDecimal<#else>${mFieldDto.dataType }</#if> get${mFieldDto.fieldNameJava?cap_first}() { 
    return this.${mFieldDto.fieldNameJava?uncap_first}; 
  }	
	
  public void set${mFieldDto.fieldNameJava?cap_first}(<#if mFieldDto.dataType == 'Datetime'>Date<#elseif mFieldDto.dataType == 'Number'>BigDecimal<#else>${mFieldDto.dataType }</#if> ${mFieldDto.fieldNameJava?uncap_first}) {
    this.${mFieldDto.fieldNameJava?uncap_first} = ${mFieldDto.fieldNameJava?uncap_first};
  }	
	</#list>	

}