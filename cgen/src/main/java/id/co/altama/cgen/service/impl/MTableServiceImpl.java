package id.co.altama.cgen.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.altama.cgen.dao.MTableDao;
import id.co.altama.cgen.model.RestResponse;
import id.co.altama.cgen.service.MTableService;
import id.co.altama.cgen.util.DateUtils;

@Service("mTableService")
public class MTableServiceImpl implements MTableService {

	@Autowired
	private MTableDao mTableDao;

	@Override
	public RestResponse getCurrentTimestampDatabase() {
		RestResponse response = new RestResponse(RestResponse.OK_REST_STATUS, "OK");
		Date current = mTableDao.getCurrentTimestampDatabase();
		String currentStr = DateUtils.datetimeToString(current, DateUtils.FORMAT_DATE_TIME);
		response.setContents(currentStr);
		return response;
	}

}
