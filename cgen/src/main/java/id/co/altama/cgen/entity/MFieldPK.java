package id.co.altama.cgen.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Embeddable
@EqualsAndHashCode
@ToString
public class MFieldPK implements Serializable {

	private static final long serialVersionUID = 1L;

	@Setter
	@Getter
	@Column(name = "field_name", nullable = false, length = 50)
	private String fieldName;

	@Setter
	@Getter
	@Column(name = "table_name", nullable = false, length = 50)
	private String tableName;
}
