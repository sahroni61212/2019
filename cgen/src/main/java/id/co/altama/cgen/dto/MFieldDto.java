package id.co.altama.cgen.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode
@ToString
@Data
public class MFieldDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String fieldName;

	private String tableName;

	private String fieldNameJava;

	private String dataType;

	private int length;

	private String nullable = "Y";

	private String primaryKey;

	private Date createdDate = new Date();

	private String created_by;

	private Date updatedDate;

	private String updatedBy;
	
	
}
