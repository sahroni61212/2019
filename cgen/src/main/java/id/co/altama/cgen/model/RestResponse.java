package id.co.altama.cgen.model;

import java.io.Serializable;

/**
 * Template converter from web service json response to java class and vice
 * versa.
 * 
 * @author Sahroni
 * 
 */
public class RestResponse implements Serializable {
	private static final long serialVersionUID = 4210265933052852138L;
	private String status; // status flag based on CommonConstants
	private String message; // message description
	private long totalRecords; // total records data
	private Object contents; // real data content

	public static final String OK_REST_STATUS = "00";
	public static final String ERROR_REST_STATUS = "01";

	public static final String ASC = "asc";
	public static final String DESC = "desc";
	public static final String PARAM = "param";

	public static final String PAGE = "page";
	public static final String SIZE = "size";
	public static final String TOKEN = "token";
	public static final String SEARCH = "search";
	public static final String DIRECTION = "direction";
	public static final String ORDER_BY = "order_by";
	
	public RestResponse() {
	}

	public RestResponse(String status, String message) {
		this.status = status;
		this.message = message;
	}

	public RestResponse(String status, String message, Object contents) {
		this.status = status;
		this.message = message;
		this.contents = contents;
	}

	public RestResponse(String status, String message, Object contents,
			long totalRecords) {
		this.status = status;
		this.message = message;
		this.contents = contents;
		this.totalRecords = totalRecords;
	}

	public String getStatus() {
		return status;
	}

	public String getMessage() {
		return message;
	}

	public Object getContents() {
		return contents;
	}

	public void setContents(Object contents) {
		this.contents = contents;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public long getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(long totalRecords) {
		this.totalRecords = totalRecords;
	}
	
	
	public static enum HttpMethod{
		GET, POST, PUT;
	}
}