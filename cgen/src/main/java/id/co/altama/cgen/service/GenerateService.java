package id.co.altama.cgen.service;

public interface GenerateService {
	
	public String generateDto(String... tableNames) throws Exception;
	
	public String generateDtoLombok(String... tableNames) throws Exception;
	
	public String generateEntityHibernate(String... tableNames) throws Exception;
	
	public String generateEntityHibernateLombok(String... tableNames) throws Exception;
}
