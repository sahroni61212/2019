package id.co.altama.cgen.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode
@ToString
@Data
public class MTableDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String tableName;

	private String tableNameJava;

	private Date createdDate = new Date();

	private String created_by;

	private Date updatedDate;

	private String updatedBy;
	
	private List<MFieldDto> mFieldDtos = new ArrayList<MFieldDto>();
	
	private Integer countPk;

}
