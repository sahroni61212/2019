package id.co.altama.cgen.dao;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import id.co.altama.cgen.entity.MTable;

public interface MTableDao extends JpaRepository<MTable, String> {

	@Query(value = "SELECT CURRENT_TIMESTAMP", nativeQuery = true)
	public Date getCurrentTimestampDatabase();
}
