package id.co.altama.cgen.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.altama.cgen.entity.MUser;

public interface MUserDao extends JpaRepository<MUser, Integer> {

	@Query("SELECT user FROM MUser user WHERE user.userName = :username")
	public MUser findByUsername(@Param("username") String username);
}
