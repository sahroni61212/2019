package id.co.altama.cgen.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "M_User")
@EqualsAndHashCode
@ToString
@Data
public class MUser implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Setter
	@Getter
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@Setter
	@Getter
	@Column(name = "user_name", nullable = false, length = 50)
	private String userName;

	@Setter
	@Getter
	@Column(name = "password", nullable = false, length = 255)
	private String password;

	@Setter
	@Getter
	@Column(name = "account_expired")
	private Boolean account_expired;

	@Setter
	@Getter
	@Column(name = "account_locked")
	private Boolean account_locked;

	@Setter
	@Getter
	@Column(name = "credentials_expired")
	private Boolean credentialsExpired;

	@Setter
	@Getter
	@Column(name = "enabled")
	private Boolean enabled;

	@Setter
	@Getter
	@Column(name = "created_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate = new Date();

}
