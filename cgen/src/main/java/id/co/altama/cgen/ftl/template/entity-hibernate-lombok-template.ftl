package ${package};


import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 *
 *@author Sahroni
 *Menggunakan Generate
 *Pada ${.now?string["dd MMM yyyy HH:mm:ss"]}
 */

@Entity
@Table(name = "${mTableDto.tableName}")
@EqualsAndHashCode
@ToString
public class ${mTableDto.tableNameJava?cap_first} implements Serializable {

  private static final long serialVersionUID = 1L;
	
  <#list mFieldDtos as mFieldDto>
  @Setter
  @Getter
  <#if mFieldDto.dataType == 'Datetime'>@Temporal(TemporalType.TIMESTAMP)<#elseif mFieldDto.dataType == 'Date'>@Temporal(TemporalType.DATE)</#if>
  @Column(name = "${mFieldDto.fieldName}", nullable = <#if mFieldDto.nullable == 'Y'>true<#else>false</#if> <#if mFieldDto.dataType == 'String'>, length = ${mFieldDto.length}</#if>)
  private <#if mFieldDto.dataType == 'Datetime'>Date<#elseif mFieldDto.dataType == 'Number'>BigDecimal<#else>${mFieldDto.dataType }</#if> ${mFieldDto.fieldNameJava?uncap_first};
	
  </#list>
	
}