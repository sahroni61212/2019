package id.co.altama.cgen.ftl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import freemarker.template.Configuration;
import freemarker.template.Template;
import id.co.altama.cgen.dto.MTableDto;

public class GenerateTest {

	private static GenerateTest engine = new GenerateTest();
	private Template template = null;
	Map<String, Object> dataMap = new HashMap<String, Object>();

	private GenerateTest() {
		init();
	}

	private void init() {
		Configuration cfg = new Configuration();
		try {
			template = cfg.getTemplate("src/main/java/id/co/altama/cgen/ftl/template/entity-template.ftl");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static GenerateTest get() {
		return engine;
	}

	public GenerateTest buildData() {
		dataMap.put("package", "com.bla.bla");
		dataMap.put("mTableDto", new MTableDto());
		System.out.println("Preparing Data");
		return engine;
	}

	public void writeFile() {
		Writer file = null;
		try {
			file = new FileWriter(new File("/home/fedora/ftl/Bean.java"));
			template.process(dataMap, file);
			file.flush();
			System.out.println("Success");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				file.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}
}
