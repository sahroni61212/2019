package ${package.entity};


import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 *
 *@author Sahroni
 *Menggunakan Generate
 *Pada ${.now?string["dd MMM yyyy HH:mm:ss"]}
 */

public class ${mTableDto.tableNameJava?cap_first} implements Serializable {

	private static final long serialVersionUID = 1L;
	
	<#list mTableDto.mFieldDtos as mFieldDto>
	private ${mFieldDto.dataType} ${mFieldDto.fieldNameJava?uncap_first};
	
	</#list>
	
		
	<#list mTableDto.mFieldDtos as mFieldDto>
	public ${columnModel.dataType} get${mFieldDto.fieldNameJava?cap_first}() { 
		return this.${mFieldDto.fieldNameJava?uncap_first}; 
	}	
	
	public void set${mFieldDto.fieldNameJava?cap_first}(${columnModel.dataType} ${mFieldDto.fieldNameJava?uncap_first}) {
		this.${mFieldDto.fieldNameJava?uncap_first} = ${mFieldDto.fieldNameJava?uncap_first};
	}
	
	</#list>	

}